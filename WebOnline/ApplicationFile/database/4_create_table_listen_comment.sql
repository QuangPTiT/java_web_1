use toieconline;
create  table listenguideline(
  listenguidelineid bigint not null primary key auto_increment,
  title nvarchar(512)null,
  image varchar(512)null,
  content text null,
  createdate timestamp null,
  modifieddate timestamp null
);

create table comment(
  commentid bigint not null primary key auto_increment,
  cotent text,
  userid bigint null ,
  listenguidelineid bigint null,
  createdate timestamp
);