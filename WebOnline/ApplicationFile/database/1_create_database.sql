create table user
(
	userid bigint not null primary key auto_increment,
    name nvarchar(255),
    password varchar(255),
    fullname nvarchar(300),
    createdate timestamp
);
create table roler
(
	roleid bigint not null primary key auto_increment,
    name nvarchar(255)
);
