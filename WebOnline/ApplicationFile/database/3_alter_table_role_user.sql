use toieconline;
alter table user add column roleid bigint;
alter table user add constraint fk_key_role foreign key(roleid) references roler(roleid);