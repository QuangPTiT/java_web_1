<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
    <title><dec:title default="Web Page"/></title>
    <dec:head/>
</head>
<body>
<%@include file="/common/admin/header.jsp"%>
<%@include file="/common/admin/menu.jsp"%>
<dec:body/>
<%@include file="/common/admin/footer.jsp"%>
</body>
</html>
