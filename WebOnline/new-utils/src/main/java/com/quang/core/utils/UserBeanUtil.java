package com.quang.core.utils;

import com.quang.core.dto.UserDto;
import com.quang.persistence.entity.UserEntity;

public class UserBeanUtil {
    public static UserDto entityToDto(UserEntity userEntity)
    {
        UserDto userDto= new UserDto();
        userDto.setName(userEntity.getName());
        userDto.setFullName(userEntity.getFullName());
        userDto.setId(userEntity.getId());
        userDto.setPassword(userEntity.getPassword());
        userDto.setCreateDate(userEntity.getCreateDate());
        userDto.setRoleDto(RoleBeanUtil.entityToDto(userEntity.getRoleEntity()));
        return userDto;
    }

    public static UserEntity dtoToEntity(UserDto userDto)
    {
        UserEntity userEntity= new UserEntity();
        userEntity.setFullName(userDto.getName());
        userEntity.setCreateDate(userDto.getCreateDate());
        userEntity.setPassword(userEntity.getPassword());
        userEntity.setRoleEntity(RoleBeanUtil.dtoToEntity(userDto.getRoleDto()));
        userEntity.setId(userDto.getId());
        userEntity.setName(userDto.getName());
        return userEntity;
    }
}
