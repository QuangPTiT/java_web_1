package com.quang.core.utils;

import com.quang.core.dto.RoleDto;
import com.quang.persistence.entity.RoleEntity;

public class RoleBeanUtil {
    public static RoleDto entityToDto(RoleEntity rolerEntity) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(rolerEntity.getId());
        roleDto.setName(rolerEntity.getName());
        return roleDto;
    }

    public static  RoleEntity dtoToEntity(RoleDto roleDto) {
        RoleEntity rolerEntity = new RoleEntity();
        rolerEntity.setId(roleDto.getId());
        rolerEntity.setName(roleDto.getName());
        return rolerEntity;
    }
}
