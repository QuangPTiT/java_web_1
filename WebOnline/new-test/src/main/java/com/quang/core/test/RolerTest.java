package com.quang.core.test;

import com.quang.core.dao.RoleDao;
import com.quang.core.dao.UserDao;
import com.quang.core.daoimpl.RoleDaoImpl;
import com.quang.core.daoimpl.UserDaoImpl;
import com.quang.persistence.entity.RoleEntity;
import com.quang.persistence.entity.UserEntity;
import org.testng.annotations.Test;

import java.util.List;

public class RolerTest {
    @Test
    public void checkFindAll() {
//        RoleDao roleDao= new RoleDaoImpl();
//        List<RoleEntity> list= roleDao.findAll();
        UserDao userDao = new UserDaoImpl();
        List<UserEntity> list1 = userDao.findAll();
    }

    @Test
    public void checkUpdate() {
        RoleDao roleDao = new RoleDaoImpl();
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(1);
        roleEntity.setName("admin");
        roleDao.update(roleEntity);
    }

    @Test
    public void checkSave()
    {
        RoleDao roleDao= new RoleDaoImpl();
        RoleEntity roleEntity= new RoleEntity();
        roleEntity.setId(3);
        roleEntity.setName("manager");
        roleDao.save(roleEntity);
    }

    @Test
    public void checkFindById()
    {
        UserDao userDao= new UserDaoImpl();
        UserEntity userEntity= userDao.findById(2);
    }
}
