package com.quang.core.test;

import com.quang.core.dao.RoleDao;
import com.quang.core.dao.UserDao;
import com.quang.core.daoimpl.RoleDaoImpl;
import com.quang.core.daoimpl.UserDaoImpl;
import com.quang.persistence.entity.RoleEntity;
import com.quang.persistence.entity.UserEntity;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class LoginTest {

    private transient final Logger log = Logger.getLogger(this.getClass());

    @Test
    public void checkUserExits() {
        UserDao userDao = new UserDaoImpl();
        String name = "quangdothanh";
        String password = "123456";
        UserEntity userEntity = userDao.isUserExits(name, password);
        if (userEntity == null) {
            System.out.println("fail");
            log.error("Login fail");
        } else {
            System.out.println("true");
            log.error("Login pass");
        }
    }

    @Test
    public void checkDelete()
    {
        RoleDao roleDao= new RoleDaoImpl();
        RoleEntity roleEntity= new RoleEntity();
        roleEntity.setId(3);
        roleDao.delete(roleEntity);
    }
}
