package com.quang.core.web.command;

import java.util.List;

public class AbstractCommand<T> {
    protected T pojo; // vs moi T tuong ung dto-command
    private String crudaction;
    private int maxPageItems = 20;
    private int firstItem = 0;
    private int totalItems = 0;
    private String sortExpression;
    private String sortDirection;
    private List<T> listResult;
    private String tableID = "tableList";
    private String messageResponse;
    private int page = 1;

    public String getCrudaction() {
        return crudaction;
    }

    public void setCrudaction(String crudaction) {
        this.crudaction = crudaction;
    }

    public T getPojo() {
        return this.pojo;
    }

    public void setPojo(T pojo) {
        this.pojo = pojo;
    }
}
