package com.quang.core.dao;

import com.quang.core.data.dao.GenericDao;
import com.quang.persistence.entity.RoleEntity;

public interface RoleDao extends GenericDao<Integer, RoleEntity> {
}
