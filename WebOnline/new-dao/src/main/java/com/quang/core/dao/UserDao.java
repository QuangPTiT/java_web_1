package com.quang.core.dao;

import com.quang.core.data.dao.GenericDao;
import com.quang.persistence.entity.UserEntity;

public interface UserDao extends GenericDao<Integer, UserEntity> {
    UserEntity isUserExits(String name , String password);
}
