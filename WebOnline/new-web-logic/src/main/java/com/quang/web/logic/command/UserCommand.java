package com.quang.web.logic.command;

import com.quang.core.dto.UserDto;
import com.quang.core.web.command.AbstractCommand;

public class UserCommand extends AbstractCommand<UserDto> {
    public UserCommand() {
        this.pojo = new UserDto();
    }
}
