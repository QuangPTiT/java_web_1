package com.quang.web.logic.command;

import com.quang.core.dto.RoleDto;
import com.quang.core.web.command.AbstractCommand;

public class RoleCommand extends AbstractCommand<RoleDto> {
    public RoleCommand() {
        this.pojo = new RoleDto();
    }
}
