package com.quang.web.logic.controller.admin;

import com.quang.core.dto.RoleDto;
import com.quang.core.dto.UserDto;
import com.quang.core.service.UserService;
import com.quang.core.serviceimpl.UserServiceImpl;
import com.quang.core.web.utils.FormUtils;
import com.quang.web.logic.command.UserCommand;
import com.quang.web.logic.common.WebConstant;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/login.html")
public class LoginController extends HttpServlet {

    private transient final Logger log = Logger.getLogger(this.getClass());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rs = req.getRequestDispatcher("/views/web/login.jsp");
        rs.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //log.error("Vao post");
        UserCommand userCommand = FormUtils.populate(UserCommand.class, req);
        UserDto pojo = userCommand.getPojo();
        UserService userService = new UserServiceImpl();
        try {
            UserDto userDto = userService.isUserExits(pojo);
            if (userDto != null) {
                RoleDto roleDto = userDto.getRoleDto();
                if (userDto != null && roleDto != null) {
                    if (roleDto.getName().equals(WebConstant.ROLE_ADMIN)) {
                        req.setAttribute(WebConstant.ALERT, WebConstant.TYPE_SUCESS);
                        req.setAttribute(WebConstant.MESSAGE_RESPONSE, "Hi ! Chào Admin !");
                    } else {
                        if (roleDto.getName().equals(WebConstant.ROLE_USER)) {
                            req.setAttribute(WebConstant.ALERT, WebConstant.TYPE_SUCESS);
                            req.setAttribute(WebConstant.MESSAGE_RESPONSE, "Hi ! Chào Cậu !");
                        }
                    }
                }
            }
        } catch (NullPointerException ex) {
            req.setAttribute(WebConstant.ALERT, WebConstant.TYPE_ERROR);
            req.setAttribute(WebConstant.MESSAGE_RESPONSE, "Tài khoản hoặc mật khẩu sai !!!");
        }
        RequestDispatcher rs = req.getRequestDispatcher("/views/web/login.jsp");
        rs.forward(req, resp);
    }
}
