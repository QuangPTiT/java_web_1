package com.quang.web.logic.common;

public class WebConstant {
    public  static  final  String MESSAGE_RESPONSE ="messageResponse";
    public  static  final String FROM_ITEM="item";
    public  static  final String LIST_ITEMS="items";
    public  static  final String ALERT="alert";
    public  static  final String TYPE_SUCESS="Success";
    public  static  final String TYPE_ERROR="Error";
    public  static  final String ROLE_USER="user";
    public  static  final String ROLE_ADMIN="admin";
}
