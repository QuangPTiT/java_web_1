package com.quang.persistence.entity;

import javax.persistence.*;
import  java.util.List;

@Entity
@Table(name = "roler")
public class RoleEntity {
    @Id
    @Column(name="roleid")
    private int id;
    @Column(name="name")
    private String name;

    @OneToMany(mappedBy = "roleEntity",fetch = FetchType.LAZY) //map lai class role user ben class user tuong ung
    private List<UserEntity> userEntities;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
