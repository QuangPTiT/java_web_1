package com.quang.persistence.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import  java.util.List;
@Entity
@Table(name = "listenguideline")
public class ListenguidelineEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int listenguidelineId;
    @Column(name = "title")
    private String title;
    @Column(name="image")
    private String image;
    @Column(name="content")
    private String content;
    @Column(name="createdate")
    private Timestamp createDate;
    @Column(name = "modifieddate")
    private Timestamp modifieddate;

    @OneToMany(mappedBy = "listenguidelineEntity", fetch = FetchType.LAZY)
    private List<CommentEntity> commentEntities;

    public int getListenguidelineId() {
        return listenguidelineId;
    }

    public void setListenguidelineId(int listenguidelineId) {
        this.listenguidelineId = listenguidelineId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Timestamp modifieddate) {
        this.modifieddate = modifieddate;
    }
}
