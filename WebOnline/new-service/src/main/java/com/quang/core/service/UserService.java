package com.quang.core.service;

import com.quang.core.dto.UserDto;
import com.quang.persistence.entity.UserEntity;

public interface UserService {
    UserDto isUserExits(UserDto userDto);
    UserDto findRoleByUser(UserDto userDto);
}
