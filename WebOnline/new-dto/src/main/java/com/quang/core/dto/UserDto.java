package com.quang.core.dto;

import javax.management.relation.Role;
import java.io.Serializable;
import java.sql.Timestamp;

public class UserDto implements Serializable {

    private int id;

    private String name;

    private String password;

    private String fullName;

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    private Timestamp createDate;

    private RoleDto roleDto;

    public RoleDto getRoleDto() {
        return roleDto;
    }

    public void setRoleDto(RoleDto roleDto) {
        this.roleDto = roleDto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }


}
