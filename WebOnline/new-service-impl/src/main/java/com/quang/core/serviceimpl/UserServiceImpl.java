package com.quang.core.serviceimpl;


import com.quang.core.dao.UserDao;
import com.quang.core.daoimpl.UserDaoImpl;
import com.quang.core.dto.UserDto;
import com.quang.core.service.UserService;
import com.quang.core.utils.UserBeanUtil;
import com.quang.persistence.entity.UserEntity;


public class UserServiceImpl implements UserService {

    public UserDto isUserExits(UserDto userDto) {
        UserDao userDao= new UserDaoImpl();
        UserEntity userEntity= userDao.isUserExits(userDto.getName(),userDto.getPassword());
        return UserBeanUtil.entityToDto(userEntity);
    }

    public UserDto findRoleByUser(UserDto userDto) {
        UserDao userDao= new UserDaoImpl();
        UserEntity userEntity= userDao.isUserExits(userDto.getName(),userDto.getPassword());
        return UserBeanUtil.entityToDto(userEntity);
    }
}
