package com.quang.core.common.utils;

import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Exception ex) {
            System.out.println("Error create sessionfatorty");
        }
        return null;
    }

    public static SessionFactory getSesionFactory() {
        return sessionFactory;
    }
}
