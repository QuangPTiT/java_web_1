package com.quang.core.data.dao;

import java.io.Serializable;
import java.util.*;

public interface GenericDao <ID extends Serializable,T>{
    List<T> findAll();
    T update(T entity);
    void save(T entity);
    void delete(T entity);
    T findById(ID id);
}
