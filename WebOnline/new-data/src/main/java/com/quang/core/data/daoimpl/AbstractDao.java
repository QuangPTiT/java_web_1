package com.quang.core.data.daoimpl;

import com.quang.core.common.utils.HibernateUtil;
import com.quang.core.data.dao.GenericDao;
import org.hibernate.*;

import java.io.ObjectInput;
import java.lang.reflect.ParameterizedType;
import java.util.*;

import java.io.Serializable;

public class AbstractDao<ID extends Serializable, T> implements GenericDao<ID, T> {
    public List<T> findAll() {
        List<T> list = new ArrayList<T>();
        Session session = HibernateUtil.getSesionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //HQL
            StringBuilder stringBuilder = new StringBuilder("FROM ");
            stringBuilder.append(this.getPersistenceClassName());
            Query query = session.createQuery(stringBuilder.toString());
            list = query.list();
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return list;
    }

    public T update(T entity) {
        T result = null;
        Session session = HibernateUtil.getSesionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            result = (T) session.merge(entity);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return result;
    }

    public void save(T entity) {
        Session session = HibernateUtil.getSesionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.persist(entity);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public int delete(List<ID> ids) {
        int count = 0;
        Session session = HibernateUtil.getSesionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            for (ID id : ids) {
                T element = (T) session.get(this.persistenceClass, id);
                session.delete(element);
                count++;
            }
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    public T findById(ID id) {
        T result = null;
        Session session = HibernateUtil.getSesionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            result = (T) session.get(persistenceClass, id);
            if (result == null) {
                throw new ObjectNotFoundException("Not Found " + id, null);
            }
            //transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return result;
    }


    private Class<T> persistenceClass;

    public AbstractDao() {
        persistenceClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    public String getPersistenceClassName() {
        return persistenceClass.getSimpleName();
    }

}
